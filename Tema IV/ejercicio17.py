# Programa qe calcula una función entre un limite inferior y otro superior
# Muestra el uso de formateo de datos así como funciones de modulos

from math import sqrt

inf= float(input('Proporciona el limite inferios para el calculo de la función:'))
sup= float(input('Proporciona el limite superior para el calculo de la función:'))
pasos=float(input('Proporciona los incrementos que deseas utilziar:'))
x=inf
print('x'.center(6),'f(x)'.center(5),sep='|')
while x<=sup:
    f=sqrt((x+3)/x)
    print('{0:5.3} | {1:5.3}'.format(x,f))
    x+=pasos
    



               
