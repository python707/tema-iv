# Programa que muestra el uso de los recursos vistos hasta el momento.
# Este programa trata de adivinar un número entre 0 y 100 

def game():
    mini=0
    maxi=100
    num=0
    car=''
    while car!='c':
        num=(maxi+mini)//2
        print('¿Es ',num,' el número que pensaste?:')
        car=input("Teclea 'a' para indicar que es mayor.\nTeclea 'b' para indicar que es menor.\nTeclea 'c' para indicar que es correcto:")
        if car=='a':
            mini=num
        elif car=='b':
            maxi=num
        elif car=='c':
            print('Juego terminado. El número que pensaste es:',num)
        else:
            print('Perdon, No puedo procesar lo que introduciste.')

otro=True
while otro:
    input('Favor de pensar un número y presiona <enter> para continuar...')
    game()
    resp=input('¿Deseas volver a jugar:[si/no]?')
    otro=True if resp=='si' else False
