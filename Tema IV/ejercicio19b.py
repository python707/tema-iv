# Programa que muestra el uso de variables globales, locales y no locales.
# También la definición de funciones locales dentro de otra función

def miFuncion():
    def funInt1():
        miVar=2
        print('miVar Interna:',miVar)
    def funInt2():
        nonlocal miVar
        miVar=4
        print('miVar nonlocal:',miVar)
    def funInt3():
        global miVar
        miVar=6
        print('miVar global:',miVar)
    miVar=10
    print('miVar local en miFuncion:',miVar)
    funInt1()
    funInt2()
    funInt3()
    print('miVar local en miFuncion:',miVar)


miVar=12
miFuncion()
print('miVar global:',miVar)
    
